#include "SerialCanRelay.h"

SoftwareSerial mySerial(INPUT1_1, INPUT1_2); // RX, TX

void readCanFromSerial() {
    static uint16_t std_id;
    static byte data[8];
    static uint8_t len;
    static int8_t receiving_pos = -2;
    int cnt = 100;
    static bool trash;
    while(--cnt > 0 && Serial.available() > 0) {
        uint8_t c = Serial.read();
        //mySerial.println(c, HEX);
        if(receiving_pos < 0) {
            if(c == '$') {
                receiving_pos++; // start receive
                trash = false;
                digitalWrite(STAT_LED2, !digitalRead(STAT_LED2));
            }
        } else if(receiving_pos < 2) {
            memcpy((void*)&std_id + receiving_pos, &c, 1);
            receiving_pos++;
        } else if(receiving_pos == 2) {
            if(c > 8) trash = true;
            len = min(8, c);
            receiving_pos++;
        } else if(receiving_pos < 3 + len){
            data[receiving_pos - 3] = c;
            receiving_pos++;
        } else {
            if(!trash) {
                if(CanCommunication::getDestFromStdId(std_id)==self_can_addr){
                    canOnReceive(std_id, data, len);
                    mySerial.println("me");
                    mySerial.println(CanCommunication::getDestFromStdId(std_id));
                }else{
                    mySerial.println("other");
                    mySerial.println(CanCommunication::getDestFromStdId(std_id));
                    CanCom.sendData(
                        CanCommunication::getDataTypeFromStdId(std_id),
                        CanCommunication::getDestFromStdId(std_id),
                        data, len);
                }
            }
            receiving_pos = -2;
            digitalWrite(STAT_LED1, !digitalRead(STAT_LED1));
        }
    }
}
