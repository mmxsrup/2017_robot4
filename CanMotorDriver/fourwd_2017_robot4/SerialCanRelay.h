#ifndef SERIAL_CAN_RELAY_H
#define SERIAL_CAN_RELAY_H

#include <arduino.h>
#include <can_communication.h>
#include <SoftwareSerial.h>

extern const uint8_t self_can_addr;
extern void canOnReceive(uint16_t std_id, const uint8_t *data, uint8_t len);

extern SoftwareSerial mySerial;

void readCanFromSerial();

#endif
