#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <avr/wdt.h>
#include <SPI.h>
#include <mcp_can.h>
#include <can_communication.h>
#include <SerialCommand.h>

#define DEBUG

// PWM制御用のオブジェクトを生成
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// シリアルでコマンドを呼び出すために使うオブジェクトの生成
SerialCommand SCmd;


/*
 * 範囲によって値を正規化する関数
 * x([min, max]) の値を [begin, end] の範囲に正規化
 */
double normalize_range(double x, double begin, double end, double min, double max) {
#ifdef DEBUG
    Serial.print("normalize "), Serial.print((x - min) * (end - begin) / (max - min) + begin), Serial.print("\n");
#endif
    return (x - min) * (end - begin) / (max - min) + begin;
}

/*
 * Hori と Vert 用のサーボを指す位置の定数番号
 */
const uint8_t HORI = 0;
const uint8_t VERT = 1;

/* 
 * Publicなサーボ制御関数 
 */

/*
 * setServoPulse(HORI, val) に val = [1.3, 1.8] の範囲で動かす.
 * setHori(val) の val = [0.0, 180.0] を [1.3, 1.8] に正規化してsetServoPulse関数に渡す
 */
void setHori(uint8_t val) {
#ifdef DEBUG
    Serial.print("hori "), Serial.print(val), Serial.print("\n");
#endif
    setServoPulse(HORI, normalize_range(val, 1.3, 1.8, 0.0, 180.0));
}

/*
 * setServoPulse(HORI, val) に val = [0.8, 2.5] の範囲で動かす.
 * setHori(val) の val = [0.0, 180.0] を [0.8, 2.5] に正規化してsetServoPulse関数に渡す
 * setServoPulse(VERT, 80)  で一番下
 * setServoPulse(VERT, 250) で一番上
 */
void setVert(uint8_t val) {
#ifdef DEBUG
    Serial.print("vert "), Serial.print(val), Serial.print("\n");
#endif  
    setServoPulse(VERT, normalize_range(val, 0.8, 2.5, 0.0, 180.0));
}

/*
 * Privateなサーボ制御関数 (全く触ってないのでよく分からない)
 */
// you can use this function if you'd like to set the pulse length in seconds
// e.g. setServoPulse(0, 0.001) is a ~1 millisecond pulse width. its not precise!
void setServoPulse(uint8_t n, double pulse) {
    double pulselength;
  
    pulselength = 1000000;   // 1,000,000 us per second
    pulselength /= 50;   // 60 Hz
    //Serial.print(pulselength); Serial.println(" us per period"); 
    pulselength /= 4096;  // 12 bits of resolution
    //Serial.print(pulselength); Serial.println(" us per bit"); 
    pulse *= 1000;
    pulse /= pulselength;
    //Serial.println(pulse);
    pwm.setPWM(n, 0, pulse);
}
void setServoPulseWithDeg(uint8_t n, double deg){
    //0deg = 0.65 180deg = 2.47
    if(deg > 180.0){
    deg = 180.0;
    }
    if(deg < 0.0){
    deg = 0.0;  
    }
    setServoPulse(n,  ((2.47 - 0.65) / 180.0) * deg + 0.65);
}


/*
 * Can通信 で使用する定数や関数
 */
// 自分への命令かどうかを判断する定数 (サーバー側と合わせること)
const uint8_t self_can_addr = 0x04;
// どの命令か判断する定数 (サーバー側と合わせること)
const uint8_t COMMAND_HAND = 0x00;
const uint8_t COMMAND_SERVORESET = 0x01;

//データ受信時に呼ばれる関数
void canOnReceive(uint16_t std_id, const uint8_t *data, uint8_t len) {
    uint8_t type = CanCommunication::getDataTypeFromStdId(std_id);
    uint8_t src = CanCommunication::getSrcFromStdId(std_id);
    uint8_t dest = CanCommunication::getDestFromStdId(std_id);

//  Serial.print("canonReceive");
    if(dest != self_can_addr){
        return;
    }
    
    if(type == CAN_DATA_TYPE_COMMAND) {
        switch (data[0]) {
            case COMMAND_HAND:
                if(len == 3) {
                    uint8_t horiVal = data[1];
                    uint8_t vertVal = data[2];
                    setHori(horiVal);
                    setVert(vertVal);
#ifdef DEBUG    
                    Serial.print("COMMAND HAND "),
                    Serial.print(horiVal), Serial.print(" "), Serial.print(vertVal), Serial.print("\n");
#endif                    
                }
                break;
   
            case COMMAND_SERVORESET:
                Serial.println("COMMAND_SERVORESET");
                if(len == 2){
                    uint8_t isReset = data[1];
                    if(isReset == 0){
                            digitalWrite(SERVO_OFF, HIGH);
                            Serial.print("servoResetOK\n");
                    }else{
                        digitalWrite(SERVO_OFF, LOW);
                    }  
                }
                break;
        }
        digitalWrite(STAT_LED2, !digitalRead(STAT_LED2));
  }
}

/*
 * PCと通信するための関数
 */
void parseError(const char* message) {
    Serial.print("parse error in "), Serial.println(message);
}
void parseHori() {
    char *arg = SCmd.next();
    if (arg != NULL) {
        uint8_t val = static_cast<uint8_t>(atoi(arg));
        setHori(val);
    } else {
        parseError("parseHori()");
    }
}
void parseVert() {
    char *arg = SCmd.next();
    if (arg != NULL) {
        uint8_t val = static_cast<uint8_t>(atoi(arg));
        setVert(val);
    } else {
        parseError("parseVert()");
    }
}
// テスト用関数
void test() {
    char *arg = SCmd.next();
    if (arg != NULL) {
        uint8_t val = static_cast<uint8_t>(atoi(arg));
        Serial.println(val);
        setServoPulse(0, (double)val / 100.0);
        Serial.println((double)val / 100.0);
    } else {
        parseError("parseVert()");
    }
}


void setup() {
    // put your setup code here, to run once:
    pinMode(STAT_LED1, OUTPUT);
    pinMode(STAT_LED2, OUTPUT);
    pinMode(SERVO_OFF, OUTPUT);
    digitalWrite(SERVO_OFF, LOW);
    Serial.begin(115200);

    // Pwn 設定
    pwm.begin();
    pwm.setPWMFreq(50); // 1kHz ?

    // Can 通信の設定
    while(CanCom.begin(self_can_addr, CAN_250KBPS) != CAN_OK);
    CanCom.onReceive(canOnReceive); // 割り込み登録

    // PCで制御するための設定
    SCmd.addCommand("Hori", parseHori);
    SCmd.addCommand("Vert", parseVert);
    SCmd.addCommand("v", test);
    Serial.println("* * * Command List * * *");
    Serial.println("Hori <val>: call setHori(uint8_t val)");
    Serial.println("Vert <val>: call setVert(uint8_t val)");
    Serial.println("Test <val>: call setServoPulse(2, (double)val / 100.0)");
    delay(100);
}

uint16_t cnt = 0;
void loop() {
    // put your main code here, to run repeatedly:
    CanCom.tasks();
    SCmd.readSerial();
    digitalWrite(STAT_LED1,   cnt%2 );
    digitalWrite(STAT_LED2, !(cnt%2));
}
