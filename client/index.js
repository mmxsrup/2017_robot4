﻿/* ? ms ごとに呼び出す関数を登録 */

/*
 * SendToServer()  := camArm と wd4 オブジェクトの値をサーバーに送る
 * UpdateDisplay() := camArm と wd4 オブジェクトの値の一部を クライアントに表示
 * checkGamepads() := gamepadの値を読み取る
 */
 
setInterval("SendToServerforwd4()", 100);
// setInterval("SendToServerforcamArm()", 100);
setInterval("UpdateDisplay()", 100);
setInterval("checkGamepads()", 100);
