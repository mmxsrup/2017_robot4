﻿/* サーバー(pc) のipアドレスを設定 */
var SERVER_IP = "localhost:8080";
var SERVER_URL = "ws://" + SERVER_IP;


/*
 * Gamepad Tester (http://html5gamepad.com/)
 * 上記のサイトを利用し, AXISのindexがどこに割り当てられているかを登録 
 * OSやブラウザの環境依存が激しいためここで設定
 * AXIS_MOVE_R := 右の足回りのAxisの番号
 * AXIS_MOVE_L := 左の足回りのAXisの番号
 * BUTTON_STOP := Stopボタン
 * BUTTON_FREE := Freeボタン
 */
var AXIS_MOVE_R = 3;
var AXIS_MOVE_L = 1;
var BUTTON_STOP = 7;
var BUTTON_FREE = 6;
var isStop = 0;
var isFree = 1;

/*
 * joystickを前方へ倒した場合にGamepad Tester(http://html5gamepad.com/) が -1になる時 false
 * そうでなければ true
 */
var WD4_FLAG = false;


/*
 * カメラアームを動かすための十字keyボタンの登録 
 * (サーボ自体は絶対座標で制御が, R(右), L(左), U(上), D(下) で今の位置よりどちらがわに動かすかを操作できるようにする)
 * BUTTON_HORI_R := 水平方向右側へ動かすボタン番号
 * BUTTON_HORI_L := 水平方向左側へ動かすボタン番号
 * BUTTON_VERT_U := 上下方向上側へ動かすボタンの番号
 * BUTTON_VERT_U := 上下方向下側へ動かすボタンの番号
 */
var BUTTON_HORI_R = 3;
var BUTTON_HORI_L = 1;
var BUTTON_VERT_U = 0;
var BUTTON_VERT_D = 2;


/*
 * カメラアームを動かすボタンを一度押したら何度動かすかを設定
 * CAM_SERVO_DIFF := 一度で変更する値
 */
var CAM_SERVO_DIFF = 50;


/*
 * TPIP2のボードからPWMを出力
 * ピンのインデックスを指定
 */
var HORI_INDEX = 0;
var VERT_INDEX = 1;



