﻿var ws = new WebSocket(SERVER_URL, "control");

/*
 * カメラ制御用サーボのオブジェクト
 * hori : 0 ~ 255
 * vert : 0 ~ 255
 */
var camArm = {
    "cam_arm" : {
        "hori" : 90,
        "vert" : 0,
    }
}

/*
 * 足回り制御用のオブジェクト
 * left  : -255 ~ 255
 * right : -255 ~ 255 
 */
var wd4 = {
    "4wd" : {
        "stop" : false,
        "free" : false,
        "go" : {
            "left" : 0,
            "right" : 0,
        },
    },
}

/* 状態をjson形式でサーバーに送る */
function SendToServerforcamArm(){
    console.log("sendcamArm");
    ws.send(JSON.stringify(
        camArm,
    ));
}
function SendToServerforwd4(){
    console.log("sendwd4");
    ws.send(JSON.stringify(
        wd4,
    ));
}

/* クライアントの表示をアップデート */
function UpdateDisplay() {
    $("#cameraHori").html(camArm.cam_arm.hori);
    $("#cameraVert").html(camArm.cam_arm.vert);
    $("#moveR").html(wd4["4wd"].go.right);
    $("#moveL").html(wd4["4wd"].go.left);

    $("#statuscam").empty();
    $("#statuswd4").empty();
    $("#statuscam").append(JSON.stringify(camArm));
    $("#statuswd4").append(JSON.stringify(wd4));

}

function ParseDataAxes(arg) {
    if (isStop) {
        wd4["4wd"].go.right = 0;
        wd4["4wd"].go.left = 0;
        wd4["4wd"].stop = true;
        wd4["4wd"].free = false;
        return;
    }

    if (isFree) {
        wd4["4wd"].go.right = 0;
        wd4["4wd"].go.left = 0;
        wd4["4wd"].stop = false;
        wd4["4wd"].free = true;
        return;
    }

    for (var index = 0; index < arg.length; index++) {
        data = arg[index];
        switch (index) {
            case AXIS_MOVE_R : 
                /* 整数値にする必要がある wd4_flagに合わせて 正負を調整 */
                if(WD4_FLAG) wd4["4wd"].go.right = Math.ceil(Math.sign(data)*Math.pow(data, 2) *  255.0);
                else         wd4["4wd"].go.right = Math.ceil(Math.sign(data)*Math.pow(data, 2) * -255.0);
                break;
            case AXIS_MOVE_L : 
                /* 整数値にする必要がある wd4_flagに合わせて 正負を調整 */
                if(WD4_FLAG) wd4["4wd"].go.left = Math.ceil(Math.sign(data)*Math.pow(data, 2) *  255.0);
                else         wd4["4wd"].go.left = Math.ceil(Math.sign(data)*Math.pow(data, 2) * -255.0);
                break;
        }
    }
    wd4["4wd"].stop = false;
    wd4["4wd"].free = false;
}

function ParseDataButton(arg, index) {
    if(!arg) return; /* ボタンが押されていない場合 */
    switch (index) {
        /* 指定されたボタンに応じて*/
        case BUTTON_HORI_R : 
            camArm.cam_arm.hori += CAM_SERVO_DIFF;
            camArm.cam_arm.hori = Math.min(300, camArm.cam_arm.hori);
            break;
        case BUTTON_HORI_L :
            camArm.cam_arm.hori -= CAM_SERVO_DIFF;
            camArm.cam_arm.hori = Math.max(-300, camArm.cam_arm.hori);
            break;
        case BUTTON_VERT_U :
            camArm.cam_arm.vert -= CAM_SERVO_DIFF;
            camArm.cam_arm.vert = Math.max(0, camArm.cam_arm.vert);
            break;
        case BUTTON_VERT_D : 
            camArm.cam_arm.vert += CAM_SERVO_DIFF;
            camArm.cam_arm.vert = Math.min(1024, camArm.cam_arm.vert);
            break;
        case BUTTON_STOP :
            isFree = isStop ? isFree : 0;
            isStop = isStop ? 0 : 1;
            break;
        case BUTTON_FREE :
            isStop = isFree ? isStop : 0;
            isFree = isFree ? 0 : 1;
    }
}

function updateHori() {
    var pwm  = {};
    var data = camArm.cam_arm.hori;
    //var val  = normalize_range(data, -300, 300, 0, 180);
    console.log("hori", data);//, val);
    pwm[HORI_INDEX] = data;
    console.log(JSON.stringify({
        pwm: pwm
    }));
    ws.send(JSON.stringify({
        pwm: pwm
    }));
}
function updateVert() {
    var pwm  = {};
    var data = camArm.cam_arm.vert;
    //var val  = normalize_range(data, 1024, 0, 0, 180);
    console.log("vert", data);//, val);
    pwm[VERT_INDEX] = data;
    console.log(JSON.stringify({
        pwm: pwm
    }));
    ws.send(JSON.stringify({
        pwm: pwm
    }));
}

function updateArm() {
    updateHori();
    updateVert();
}