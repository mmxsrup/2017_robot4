/*
 * 範囲によって値を正規化する関数
 * x([min, max]) の値を [begin, end] の範囲に正規化
 */
function normalize_range(x, begin, end, min, max) {
	return (x - min) * (end - begin) / (max - min) + begin;
}

/*
 * pwm 0.5 := 1200
 * pwm 1.5 := 0
 * pwm 2.5 := -1200
 * の値に対応する
 * Hori : 1.3(240) ~ 1.8(-360)
 * Vert : 0.8(1024) ~ 2.5(0)
 */
 $("#cameraArmHori").click(function() {
    var pwm  = {};
    var data = parseInt($("#horival").val(), 10);
    camArm.cam_arm.hori = data;
    //var val  = normalize_range(data, 240, -360, 0, 180);
    console.log("hori", data);//, val);
    pwm[HORI_INDEX] = data;
    console.log(JSON.stringify({
        pwm: pwm
    }));
    ws.send(JSON.stringify({
        pwm: pwm
    }));
})
$("#cameraArmVert").click(function() {
    var pwm  = {};
    var data = parseInt($("#vertval").val(), 10);
    camArm.cam_arm.vert = data;
    //var val  = normalize_range(data, 840, -1200, 0, 180);
    console.log("vert", data);//, val);
    pwm[VERT_INDEX] = data;
    console.log(JSON.stringify({
        pwm: pwm
    }));
    ws.send(JSON.stringify({
        pwm: pwm
    }));
})
$("#sendpwm").click(function() {
    var pwm = {};
    pwm[1] = parseInt($("#pwmval").val(), 10); // どこに送るかは変更すること
    console.log(JSON.stringify({
        pwm: pwm
    }));
    ws.send(JSON.stringify({
        pwm: pwm
    }));
})