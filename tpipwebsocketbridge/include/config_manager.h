#ifndef CONFIG_MANAGER_H
#define CONFIG_MANAGER_H

#include "jansson.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif
    
bool config_init(const char *rel_path);
json_t* config_get(const char *key);
    
#ifdef __cplusplus
}
#endif

#endif

