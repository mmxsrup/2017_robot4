# if you call cmake with -DTARGET=TPIP2, following variables will be used.

# You have to install following package.
# http://wiki.tpip-dev.org/upload/f/fe/Tplx_install.tar.zip
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_C_COMPILER /opt/3.4.5/usr/local/bin/sh4-linux-gcc)
set(CMAKE_FIND_ROOT_PATH /opt/3.4.5/usr/local/sh4-linux/)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
