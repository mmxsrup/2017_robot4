#ifndef TPIP3_H
#define TPIP3_H

#ifdef _WIN32

#include <windows.h>

#include "TPGC_11dll.h"
#include "TPGM_12dll.h"
#include "TPJT3.h"
#include "TPSC_12dll.h"
#include "TPSI_10dll.h"
#include "TPUD_13dll.h"

#endif

#endif
