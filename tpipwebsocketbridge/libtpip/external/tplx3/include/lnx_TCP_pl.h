/** 
 * TCP communication class Driver(for Linux) 
 * @file lnx_TCP_pl.h
 * @brief TCP communication のパラメータ型ドライバー（Linux版）
 *
 * @author Katayama
 * @date 2008-02-20
 *
 * @version ver 1.00 2008/06/24
 * @version ver 1.01 2014/01/07
 *
 * Copyright (C) 2008 - 2014 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */

/** \mainpage
 * このモジュールはTCPで通信するクラス関数です。
 */
#ifndef ___LNX_TCP_PL_H___
#define ___LNX_TCP_PL_H___

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

typedef int SOCKET;
typedef unsigned long DWORD;
#define INVALID_SOCKET -1
#define MAXPENDING 1

typedef struct LNX_TCP {
	char ip_adr[16];
	int  sv_flg;
	int  port_no;
	int  rcv_tm;
	int  init_flg;				//	初期化完了フラグ(0:未完、1:完了)
	SOCKET servSocket;
	SOCKET destSocket;
	struct sockaddr_in servSockAddr;
	struct sockaddr_in destSockAddr;
} LNX_TCP_t;

extern void lnx_tcp_init(struct LNX_TCP* p, char* sv_cl);		// 初期化 sv_cl= "TCP_C":client "TCP_S":server
extern int  lnx_tcp_open(struct LNX_TCP* p, char* host_adr, int port_no, int rcv_tm); //  open & 初期化関数
extern int  lnx_tcp_retry(struct LNX_TCP* p);			// 初期化再試行関数
extern void lnx_tcp_close(struct LNX_TCP* p);			// 終了関数
extern int  lnx_tcp_send(struct LNX_TCP* p, void* send_buf, unsigned int send_size);	//	送信関数
extern int  lnx_tcp_recv(struct LNX_TCP* p, void* rcv_buf , unsigned int r_cnt);		// 受信関数
extern int  lnx_tcp_recv_rt(struct LNX_TCP* p, void* rcv_buf ,unsigned int r_cnt ,int rcv_tm);	// 受信関数（受信タイマー）


#endif /* ifndef ___LNX_TCP_PL_H___ */
