/** 
 * UDP communication class Driver(for Linux) 
 * @file W32_UDP_pl.h
 * @brief UDP communication のパラメータ型ドライバー（Linux版）
 *
 * @author Katayama
 * @date 2008-02-20
 * @version 1.00 2007/11/21 katayama
 * @version 1.10 2014/09/10 katayama
 *
 * Copyright (C) 2008 - 2014 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */

/** \mainpage
 * このモジュールはUDPで通信するクラス関数です。
 */
#ifndef ___LNX_UDP_PL_H___
#define ___LNX_UDP_PL_H___

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

typedef int SOCKET;
typedef unsigned long DWORD;
#define INVALID_SOCKET -1

typedef struct LNX_UDP {
	char ip_adr[16];			// IP address
	int  sv_flg;				// 0:client mode , 1:sever mode
	int  port_no;				// UDP port number
	int  rcv_tm;				// recieve wait time
	int  init_flg;				// initialize flag(0:incomplete、1:complete)
	SOCKET destSocket;			// destination socket
	struct sockaddr_in destSockAddr;	// destination address
//	WSADATA data;
} LNX_UDP_t;

extern int  lnx_udp_init(LNX_UDP_t* p, char* sv_cl);		/// 初期化 sv_cl= "UDP_C":client "UDP_S":server
extern int  lnx_udp_open(LNX_UDP_t* p, char* host_adr, int port_no, int rcv_tm); ///  open & 初期化関数
extern int  lnx_udp_retry(LNX_UDP_t* p);			/// 初期化再試行関数
extern void lnx_udp_close(LNX_UDP_t* p);			/// 終了関数
extern int  lnx_udp_send(LNX_UDP_t* p, void* send_buf, unsigned int send_size);	///	送信関数
extern int  lnx_udp_recv(LNX_UDP_t* p, void* rcv_buf , unsigned int r_cnt);		/// 受信関数
extern int  lnx_udp_recv_rt(LNX_UDP_t* p, void* rcv_buf ,unsigned int r_cnt ,int rcv_tm);	/// 受信関数（受信タイマー）


#endif /* ifndef ___LNX_UDP_PL_H___ */
