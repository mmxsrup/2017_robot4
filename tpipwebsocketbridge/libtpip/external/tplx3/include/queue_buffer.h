/*
 * @file   queue_buffer.h
 * @brief  Queue buffer control Header file
 *
 * @author Katayama
 * @date 2013-03-12
 * @version 1.00 : 2013/03/12 katayama
 * @version 1.01 : 2013/05/21 katayama
 *
 * Copyright (C) 2013 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 *
 */
#ifndef __QUEUE_BUFFER_H__
#define __QUEUE_BUFFER_H__

#define QUE_SIZE 1024
typedef struct {
	unsigned long put_ptr;
	unsigned long get_ptr;
	unsigned long cnt;
	unsigned long max_size;
	unsigned char *buf;
} que_t;

extern int init_que_bf(que_t* que, unsigned char* buf, int buf_sz);
extern int put_que_byte(que_t* que, unsigned char data);
extern int put_que_bf(que_t* que, unsigned char* buf, int data_sz);
extern int get_que_bf(que_t* que, unsigned char* buf, int buf_sz);
extern int get_que_byte(que_t* que);

extern int get_que_c(que_t* que);
extern void get_que_update(que_t* que);
extern int get_que_size(que_t* que);


#ifdef __DEBUG_QUE__
extern void entry_chk_que(que_t* que);
#endif

#endif // #ifndef __QUEUE_BUFFER_H__
