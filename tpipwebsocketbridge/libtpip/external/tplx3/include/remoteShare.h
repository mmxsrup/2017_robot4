/** 
 * @file remoteShare.h
 * @brief UDP remote shared memory header file
 *
 * @author  Katayama
 * @date    2014-09-10
 * @version 1.00 2014/09/10
 *
 * Copyright (C) 2014 TPIP User Community All rights reserved.
 * このファイルの著作権は、TPIPユーザーコミュニティの規約に従い
 * 使用許諾をします。
 */
#ifndef __REMOTESHARE_H__
#define __REMOTESHARE_H__

extern int rShare_open(int port_no, int mon_tm);
extern int rShare_close(void);
extern int rShare_put(char* buf, int size);
extern int rShare_get(char* buf, int buf_sz);
extern int rShare_get_status(void);

#endif // #ifndef __REMOTESHARE_H__
