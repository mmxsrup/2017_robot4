#include <cstring>
#include <cmath>
#include <cstdlib>
#include <chrono>
#include <algorithm>
#include <iostream>
#include "tpip.h"

namespace {
char ip_[16];
int sample_rate_;
int channel_num_;
int buf_size_;
const double SOUND_FREQ[] = {0, 262, 294, 330, 349, 392, 440, 494, 523};
const int DONGURI[] = {5,3,3,4,3,2,1,5,3,3,2,0,3,5,5,6,6,6,8,4,5,0,5,5,3,3,4,3,2,1,5,3,3,2,0,5,3,6,5,5,6,6,7,7,8,0,0,0,0};
}

bool tpip_connect_sound(const char *ip, int sample_rate, int channel_num, int buf_size) {
    strncpy(ip_, ip, sizeof(ip_) - 1);
    sample_rate_ = sample_rate;
    channel_num_ = channel_num;
    buf_size_ = buf_size;
    return TPIP_OK;
}

bool tpip_close_sound() {
    return TPIP_OK;
}

bool tpip_is_sound_in_connected() {
    return TPIP_OK;
}

bool tpip_is_sound_out_connected() {
    return TPIP_OK;
}


bool tpip_receive_sound(struct tpip_sound_data *data) {
    using namespace std;
    static auto pre = chrono::system_clock::now();
    auto now = chrono::system_clock::now();
    static int donguri_cnt = 0, donguri_cnt2 = 0;
    static unsigned int time_cnt = 0;
    int duration_ms = chrono::duration_cast<chrono::milliseconds>(now - pre).count();
    pre = now;
    duration_ms = min(5000, duration_ms);
    std::cerr << "duration " << duration_ms << std::endl;
    data->sample_rate = 32000;
    data->channel_num = 1;
    data->format = TPIP_SOUND_FORMAT_DEFAULT;
    data->size = 2 * (double)data->sample_rate * duration_ms / 1000.0;
    if(!tpip_sound_data_reserve(data, data->size)) return TPIP_ERROR;
    for(int i = 0; i * 2 + 1 < data->size; i++) {
        if(++donguri_cnt2 > 3200 * 2) {
            donguri_cnt = (donguri_cnt + 1) % (sizeof(DONGURI) / sizeof(DONGURI[0]));
            donguri_cnt2 = 0;
        }
        int16_t sound = sin(2 * 3.14 * SOUND_FREQ[DONGURI[donguri_cnt]] * (time_cnt++) / 32000) * 32767;
        memcpy(&data->data[i * 2], &sound, sizeof(sound));
    }
    return TPIP_OK;
}

bool tpip_clear_sound_in_buffer() {
    return TPIP_OK;
}

bool tpip_send_sound(const struct tpip_sound_data *data) {
    return TPIP_OK;
}

int tpip_get_sound_sample_rate() {
    return sample_rate_;
}

int tpip_get_sound_channel_num() {
    return channel_num_;
}

int tpip_get_sound_in_volume() {
    return 0;
}

int tpip_get_sound_out_volume() {
    return 0;
}

int tpip_get_sound_received_size() {
    return 0;
}

bool tpip_set_sound_in_volume(int volume) {
    return TPIP_OK;
}

bool tpip_set_sound_out_volume(int volume) {
    return TPIP_OK;
}

