

#ifndef TPIP_PRIVATE_H
#define TPIP_PRIVATE_H

#include <stdio.h>
#include <stdlib.h>

// #include "tpip_config.h"
// #include "tpip_hardware.h"

#define TPIP_MIN(x, y) ( (x) < (y) ? (x) : (y))
#define TPIP_MAX(x, y) ( (x) < (y) ? (x) : (y))

#define TPIP_NORMALIZE(x, a, b) ((x) < (a) ? (a) : \
                                 (x) > (b) ? (b) : (x))

#endif
