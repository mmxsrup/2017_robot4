
#include <string.h>

#include "tpip.h"
#include "tpip2.h"
#include "tpip_private.h"

// 一応チェック
#ifndef TPJT2
#error "TPJT2 is not defined at tpjt2.c"
#endif

namespace {
bool is_inited_;
char ip_[16];
bool control_connection_, jpeg_connection_;
int camera_no_;
int jpeg_request_speed_;
struct tpip_control_data control_data_;
mctrl_can_t can_received_data_;
bool can_received_;
}

/*
 * 一般
 */ 

bool tpip_init(const char* ip) {
    if(is_inited_) return strcmp(ip_, ip) == 0 ? TPIP_OK : TPIP_ERROR;
    strncpy(ip_, ip, sizeof(ip_) - 1);
    TPJT_init(ip_, NULL);
    is_inited_ = true;
    return TPIP_OK;
}

bool tpip_is_inited() {
    return is_inited_;
}

bool tpip_close() {
    if(!is_inited_) return TPIP_ERROR;
    TPJT_close();
    is_inited_ = false;
    return TPIP_OK;
}

int tpip_get_wlink_quality() {
    return TPJT_Get_Wlink();
}

bool tpip_is_controlboard_working() {
    int mask = 0b11001001;
    return TPJT_Get_stat() & mask == mask; 
}

const char* tpip_get_target_ip() {
    return ip_;
}

/*
 * 制御
 */

bool tpip_connect_control() {
    TPJT_set_com_req( jpeg_connection_ ? 0b11 : 0b10, 0);
    control_connection_ = true;
    return TPIP_OK;
}

bool tpip_close_control() {
    TPJT_set_com_req( jpeg_connection_ ? 0b01 : 0b00, 0);
    control_connection_ = false;
    return TPIP_OK;
}

// リモート接続系なら接続できているかどうか、内部系なら初期化に成功しているかどうか
// true ならば制御可能状態、false なら制御不可状態を示す、Jpeg通信は関係ない
bool tpip_is_control_connected() {
    return (TPJT_get_com_stat() & 0b10) != 0;
}

bool tpip_get_sensor(struct tpip_sensor_data *data) {
    struct INP_DT_STR sens;    
    TPJT_get_sens(&sens, sizeof(sens));
    for(int i = 0; i < TPIP_DIGITAL_IN_COUNT; i++){
        data->digital[i] = (sens.DI & (1 << i)) ? true : false;
    }
    for(int i = 0; i < TPIP_ANALOG_IN_COUNT; i++){
        data->analog[i] = sens.AI[i];
    }
    data->pulse[0] = sens.PI1;
    data->pulse[1] = sens.PI2;
    data->battery = sens.batt;
    return TPIP_OK;
}

bool tpip_set_control(const struct tpip_control_data *data) {
    memcpy(&control_data_, data, sizeof(control_data_));
	struct OUT_DT_STR ctl = { 0 };
    for(int i = 0; i < TPIP_DIGITAL_OUT_COUNT; i++){
        ctl.d_out |= ((data->digital ? 1 : 0) << i);
    }
    for(int i = 0; i < TPIP_PWM_OUT_COUNT; i++){
		ctl.PWM[i] = data->pwm[i];
    }
    ctl.MT = data->motor[0];
    TPJT_set_ctrl(&ctl , sizeof(ctl));
	return true;
}

bool tpip_get_control(struct tpip_control_data *data) {
    *data = control_data_;
    return true;
}

/*
 * 映像
 */

bool tpip_connect_jpeg() {
    TPJT_set_com_req(control_connection_ ? 0b11 : 0b01, 0);
    jpeg_connection_ = true;
    return TPIP_OK;
}

bool tpip_close_jpeg() {
    TPJT_set_com_req(control_connection_ ? 0b10 : 0b00, 0);
    jpeg_connection_ = false;
    return TPIP_OK;
}

bool tpip_is_jpeg_connected() {
    return TPJT_get_com_mode() == 4 && TPJT_get_com_stat() & 0x01; // TODO : check
}

void* tpip_get_jpeg(int *size) {
    return TPJT_get_jpeg_file(0, 0, size);        
}

bool tpip_free_jpeg() {
    TPJT_free_jpeg_file();
    return TPIP_OK;
}

bool tpip_set_jpeg_request_speed(int speed_kbps) {
    TPJT_set_vspeed(speed_kbps);
    jpeg_request_speed_ = speed_kbps;
    return TPIP_OK;
}

int tpip_get_jpeg_request_speed() {
    return jpeg_request_speed_;
}

int tpip_get_jpeg_actual_speed() {
    return TPJT_get_vspeed(); 
}
// 画像サイズ TPIP_JPEG_QVGA or VGA
bool tpip_set_jpeg_size(int size_type) {
    TPJT_set_video_inf(size_type);
    return TPIP_OK;    
}

// (要求 -> 画像データ受信の)遅延時間ms
int tpip_get_jpeg_delay_time() {
    return TPJT_Get_dtime();
}

bool tpip_set_camera_no(int no) {
    TPJT_chg_camera(no);
    camera_no_ = no;
    return TPIP_OK;
}

int tpip_get_camera_no() {
    return camera_no_;
}

/*
 * CAN通信 
 */

/// @retval 受信済みのパケット数(max 1の実装あり)
int tpip_is_can_available() {
    if(can_received_) return 1;
    else {
        int res = TPJT_Recv_CANdata(&can_received_data_);
        if(res > 0) can_received_ = true;
        return res > 0 ? 1 : 0;
    }
}

bool tpip_receive_can(struct tpip_can_data *data) {
    if(data == NULL) return TPIP_ERROR;
    if(can_received_) {
        data->standard_id = can_received_data_.STD_ID;
        data->size = TPIP_MIN(8, can_received_data_.sz);
        memcpy(data->data, can_received_data_.data, data->size);
        can_received_ = false;
        return TPIP_OK;
    } else {
        if(tpip_is_can_available()) return tpip_receive_can(data);
        else return TPIP_ERROR;
    }    
}

bool tpip_send_can(const struct tpip_can_data *data) {
    if(data == NULL) return TPIP_ERROR;
    mctrl_can_t snd_data;
    snd_data.flg = 1;
    snd_data.RTR = 0;
    snd_data.sz = TPIP_MIN(data->size, 8);
    snd_data.stat = 0;
    snd_data.STD_ID = data->standard_id;
    memcpy(snd_data.data, data->data, snd_data.sz);
    return TPJT_Send_CANdata(&snd_data, sizeof(snd_data)) > 0 ? TPIP_OK : TPIP_ERROR;
}

