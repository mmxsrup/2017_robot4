#include "tpip.h"
#include <stdio.h>
#include <time.h>
#include <string.h>

/*
 * Remote Serial通信受信速度測定
 * 別の機器から連続的にデータを送っておく
 * 1sあたりに受信したバイト数を表示する
 */
void calc_receive_speed() {
	char buf[1024 + 1];
	int size = 0;
	while (true) {
		int bytes = 0;
		int packets = 0;
		int loss = 0;
		int cnt = -1;
		struct tpip_can_data receive;
		clock_t start = clock();
		while ((double)(clock() - start) / CLOCKS_PER_SEC < 1.0) {
			if (tpip_is_can_available()) {
				if (tpip_receive_can(&receive) == TPIP_ERROR) {
					puts("receive error");
				} else {
					uint16_t new_cnt;
					memcpy(&new_cnt, receive.data + 1, 2);
					if (cnt >= 0) loss += (cnt + 1 != new_cnt);
					cnt = new_cnt;
					//printf("Receive[%d] %s %d\n", size, buf, new_cnt);
					bytes += receive.size;
					packets++;
				}
			}
		}
		clock_t end = clock();
		double time = (double)(end - start) / CLOCKS_PER_SEC;
		printf("Time %lf s | Received %d bytes | packtes %d | speed %lf bps | %lf packets/s | loss %d %lf %% \n", time, bytes, packets,  bytes / time, packets / time, loss, (double)loss / packets * 100);
	}
}

/*
 * 送信してから受信するまでの時間を計測する
 * （何らかの理由で返信が来なかったら無限に待つ）
 */
void send_and_receive_loop() {
	const char *c = "test";
	struct tpip_can_data data, receive;
	data.standard_id = 0;
	data.size = 8;
	data.data[0] = '!';
   
	while (true) {
		clock_t start = clock();
		if (tpip_send_can(&data) == TPIP_ERROR) {
			puts("send failed.");
		} else {
			while (tpip_is_can_available() == 0);
			if (tpip_receive_can(&receive) == TPIP_OK) {
				clock_t end = clock();
				printf("Receive [%lf s] %d bytes from %d \n", (double)(end - start) / CLOCKS_PER_SEC ,receive.size, receive.standard_id);
			}
			else {
				puts("receive failed.");
			}
		}
		
	}
}



int main() {
	tpip_init("192.168.0.200");
	//以下は画像ボードのポートを使うなら不要な様子（TPIP3）
	tpip_connect_control();
	printf("target : %s\n", tpip_get_target_ip());
	while (!tpip_is_control_connected());
	puts("connected");

	char c[101];
	scanf("%100s", &c);
	if(strcmp(c, "ping") == 0) send_and_receive_loop();
	else if (strcmp(c, "receive") == 0) calc_receive_speed();
	return 0;
}
