#include "lib/serial_relayed_can.h"
#include "lib/can.h"

#include "system.hpp"

bool parse_cam_arm(json_t *json) {
		json_int_t camHori;
		json_int_t camVert;
		json_t *hori = json_object_get(json, "hori");
		if (json_is_integer(hori)) {
			camHori = json_integer_value(hori);
		}		
		json_t *vert = json_object_get(json, "vert");
		if (json_is_integer(vert)) {
			camVert = json_integer_value(vert);
		}
                can_send_buffer.update(
                    (uint8_t)CAN_ID::CAM_ARM,
                    (uint8_t)CAN_COMMANDS_CAM_ARM::HAND,
                    (uint8_t)camHori,
                    (uint8_t)camVert);
		return TPIP_OK;
}
