#include <stdio.h>

#include "lib/serial_relayed_can.h"
#include "lib/can.h"

#include "system.hpp"

#define MY_MAX(x, y) ((x) > (y) ? (x) : (y))
#define MY_MIN(x, y) ((x) < (y) ? (x) : (y))


static bool parse_go(json_t *json);
static bool parse_stop(json_t *json);
static bool parse_free(json_t *json);

//
//
// public
//
//

bool parse_4wd(json_t *json) {
	if (parse_stop(json) || parse_free(json) ||
		 parse_go(json)) {
		return TPIP_OK;
	}
	return TPIP_ERROR;
}


//
//
// private
//
//

static bool parse_go(json_t *json) {
	json_t *obj = json_object_get(json, "go");
	if (json_is_object(obj)) {
        json_t *left_json = json_object_get(obj, "right");
        json_t *right_json = json_object_get(obj, "left");
        if(json_is_number(left_json) && json_is_number(right_json)) {
            int16_t left, right;
            {
                double left_d = json_number_value(left_json);
                double right_d = json_number_value(right_json);
                if(left_d > INT16_MAX || INT16_MIN > left_d) return false;
                if(right_d > INT16_MAX || INT16_MIN > right_d) return false;
                left = left_d;
                right = right_d;
            }

            left = MY_MAX(left, -255);
            left = MY_MIN(left, 255);
            right = MY_MAX(right, -255);
            right = MY_MIN(right, 255);
          
			can_send_buffer.update(
				(uint8_t)CAN_ID::WHEEL_MASTER,
				(uint8_t)CAN_COMMANDS_4WD::MOVE,
				(int16_t)left,
				(int16_t)right);

			return true;
		}
	}
	return false;
}

static bool parse_stop(json_t *json) {
	json_t *obj = json_object_get(json, "stop");
	if (json_is_true(obj)) {
		can_send_buffer.update(
			(uint8_t)CAN_ID::WHEEL_MASTER,
			(uint8_t)CAN_COMMANDS_4WD::STOP);
		return true;
	}
	return false;
}

static bool parse_free(json_t *json) {
	json_t *obj = json_object_get(json, "free");
	if (json_is_true(obj)) {
		can_send_buffer.update(
			(uint8_t)CAN_ID::WHEEL_MASTER,
			(uint8_t)CAN_COMMANDS_4WD::FREE);
		return true;
	}
	return false;
}

