#ifndef CAN_CAN_H
#define CAN_CAN_H

#include "tpip.h"

#ifdef __cplusplus
extern "C" {
#endif
    
enum CAN_TYPE {
    CAN_TYPE_EMERGENCY = 0,
    CAN_TYPE_COMMAND = 1,
    CAN_TYPE_RESPONSE = 2,
    CAN_TYPE_DATA = 3,
    CAN_TYPE_HEARBEAT = 7
};

#define CAN_ADDR_BROADCAST (0x0)

uint16_t can_generate_standard_id(uint16_t type, uint16_t src, uint16_t dest);
uint8_t can_get_message_type_from_id(uint16_t standard_id);
uint8_t can_get_src_from_id(uint16_t standard_id);
uint8_t can_get_dest_from_id(uint16_t standard_id);

#ifdef __cplusplus
}
#endif
    
#endif
