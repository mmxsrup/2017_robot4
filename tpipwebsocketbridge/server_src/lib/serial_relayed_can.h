#ifndef SERIAL_RELAYED_CAN_H
#define SERIAL_RELAYED_CAN_H

#include "tpip.h"



/*
 * can通信データをシリアル通信で中継して送るライブラリ
 * フォーマット
 * #[standard id][data size][data]
 * # : CANデータ先頭を表す
 * [standard id] : uint16_t
 * [data size] : uint8_t
 * [data] : uint8_t array (0 ~ 8 byte)
 */

bool serial_relayed_can_send(const struct tpip_can_data *data);

// （tasksでデコードしたデータから）データを取り出す
bool serial_relayed_can_receive(struct tpip_can_data *data);

// serial recieve -> can パケットにデコード の処理をする
// 定期的に呼び出すこと
bool serial_relayed_can_tasks();
// receiveしたデータパケット数を返す
int serial_relayed_can_available();

    
#endif

