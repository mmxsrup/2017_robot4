/**
 * 
 * 各ロボット用の制御を書く
 * テンプレートではc++にしてあるが、Cでも良い
 *
 * 実装が必要な関数
 * int robot_setup()
 * int robot_loop()
 * int robot_on_receive(const json_t *rcv_json)
 * int robot_on_send(json_t *send_json)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <array>
#include <thread>

#include "robot.h"          // include必須(robot_...が宣言されている)
#include "jansson.h"        // jsonパーサーライブラリ
#include "tpip.h"           // tpipライブラリのラッパー関数群
#include "tpip_json.h"      // tpipのデータをjson_tで取得・設定できる
#include "config_manager.h" // config.jsonから設定を読み込み・取得する

#include "lib/serial_relayed_can.h"
#include "lib/can.h"

#include "system.hpp"

struct BoardStatus {
    clock_t alive;    
};


std::array<BoardStatus, (int)CAN_ID::CAN_BOARD_NUM> board_status;

SendBuffer<(uint8_t)CAN_ID::TPIP> can_send_buffer;

/**
 *
 * TPIP API 初期化直後に呼ばれる関数
 * 初期位置等の設定をすると良い
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_setup() {
	tpip_connect_serial(json_string_value(config_get("ip")) , 9000);
    return 0;
}


/**
 *
 * 定期的に呼ばれる関数
 * ただし呼ばれる間隔は他の処理の時間によって変動する
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_loop() {
	static int cnt = 0;
	/*
	serial_relayed_can_tasks();
    while(serial_relayed_can_available()) {
        struct tpip_can_data data;
        if(serial_relayed_can_receive(&data)) {
            uint8_t type = can_get_message_type_from_id(data.standard_id);
            uint8_t src = can_get_src_from_id(data.standard_id);
            uint8_t dest = can_get_dest_from_id(data.standard_id);
            if(dest == (uint8_t)CAN_ID::TPIP || dest == 0) {
                fprintf(stderr, "CAN Type %d Src %d Dest %d Len %d\n",
                        type, src, dest, data.size);
                if(type == CAN_TYPE_HEARBEAT) {
					if (src < board_status.size()) {
						board_status[src].alive = clock();
					}
                }
            }
        }
    }
	*/

	static auto prev = std::chrono::steady_clock::now();
	auto now = std::chrono::steady_clock::now();
	if (now - prev > std::chrono::milliseconds(90)) {
		//std::cerr << cnt++ << std::endl;
		can_send_buffer.send();
		prev = now;
	}
    return 0;
}


/**
 *
 * 制御情報を受信した時に呼ばれる関数
 *
 * rcv_json : 受信したjsonデータ 向こうで解放されるので勝手にdecrefしないで。
 *
 * return : -1でエラー（接続を打ち切る）
 *           0で正常
 *
 */
int robot_on_receive(const json_t *rcv_json) {
    tpip_set_control_by_json(rcv_json);
    
    const json_t *root = rcv_json;    

    json_t *reset = json_object_get(root, "reset");
    if(json_is_boolean(reset)) {
        struct tpip_control_data ctl;        
        tpip_get_control(&ctl);
        ctl.digital[0] = json_boolean_value(reset);
        tpip_set_control(&ctl);
    }
    
    json_t *wd4 = json_object_get(root, "4wd");
    if(json_is_object(wd4)) {
        parse_4wd(wd4);
    }

    json_t *cam_arm = json_object_get(root, "cam_arm");
    if (json_is_object(cam_arm)) {
        parse_cam_arm(cam_arm);
    }

    return 0;
}

/**
 *
 * センサ情報を情報を送信する前に呼ばれる関数
 *
 * send_json : 送信するjsonデータ 制御ボードのセンサ情報が格納されている状態
 *             !! send_json に追加する値は全て所有権を譲渡しないといけない(_newな関数でaddする)
 * return : -1でエラー（送信しない）
 *           0で正常
 *
 */
int robot_on_send(json_t *send_json) {
    {
        json_t *tpip_sensor_json = tpip_get_sensor_as_json();
        json_object_update(send_json, tpip_sensor_json);
        json_decref(tpip_sensor_json);
    }
    json_t *board_status = json_array();
    // 送信するデータを追加する処理や、送信するデータを消してなかったことにする処理など
    if(tpip_is_can_available() > 0) {
        struct tpip_can_data can_data;
        if(tpip_receive_can(&can_data) == TPIP_OK) {
            switch(can_get_message_type_from_id(can_data.standard_id)) {
                case CAN_TYPE_HEARBEAT:
                    json_array_append_new(board_status,
                                          json_integer(can_get_src_from_id(can_data.standard_id)));
                    break;
                default:
                    break;
            }
        }
    }
    json_object_set_new(send_json, "board_status", board_status);
    return 0;
}


int main() {
    return server_main();
}
