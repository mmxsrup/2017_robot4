#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <stdint.h>
#include <cstring>
#include <cassert>
#include <map>
#include <iostream>

#include "jansson.h"
#include "tpip.h"
#include "lib/can.h"
#include "lib/serial_relayed_can.h"
#include "lib/buffer.hpp"

/*
 * ボード定義
 */

enum class CAN_ID : uint8_t {
    BROAD_CAST ,
    TPIP ,
    WHEEL_MASTER,
    WHEEL_SLAVE,
    CAM_ARM,
    RESCUE_BOARD,
    CAN_BOARD_NUM
};

static_assert((int)CAN_ID::CAN_BOARD_NUM <= 15, "CAN BOARD ID should be less than 15");


enum class CAN_COMMANDS_4WD : uint8_t {
	RESET,
    MOVE,
    STOP,
    FREE
};


extern bool parse_4wd(json_t *json);


/*
 * レスキューボードコマンド
 */
 

enum class CAN_COMMANDS_RESCUE_BOARD : uint8_t {
    GO = 1,
    ROLL
};


enum class CAN_COMMANDS_ARM_MOTOR : uint8_t {
	MOTOR = 1
};

enum class CAN_COMMANDS_SERVO : uint8_t {
	RESET = 0,
	ARM = 1,
	CAMERA = 2
};

enum class CAN_COMMANDS_CAM_ARM : uint8_t {
  HAND = 0,
  RESET = 1
};

extern bool parse_arm(json_t *json);

extern bool parse_cam_arm(json_t *json);

extern SendBuffer<(uint8_t)CAN_ID::TPIP> can_send_buffer;

#endif
