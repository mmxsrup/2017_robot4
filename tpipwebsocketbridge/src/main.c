#include <signal.h>
#include <stdio.h>

#include "tpip.h"

#include "robot.h"
#include "server/server.h"
#include "config_manager.h"
#include "interval_timer.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

static volatile bool force_exit;

struct lws_protocols protocols[5];

struct lws_context* server_init() {
    protocols[0] = server_protcol_http;
    protocols[1] = server_protcol_control;
#if defined(TPJT2) || defined(TPJT3) || defined(TPIP_EMULATOR)
    protocols[2] = server_protcol_camera;
    protocols[3] = server_protcol_sound;
#endif
    
    struct lws_context_creation_info info = {0};
    info.port = json_integer_value(config_get("port"));
    info.iface = NULL;
    info.protocols = protocols;
    info.gid = -1;
    info.uid = -1;
    info.options = 0;
    struct lws_context *context = lws_create_context(&info);
    if(context == NULL) {
        puts("server create context failed.");
        exit(-1);
    }
    server_init_http(context,
                     json_string_value(config_get("public")));
    server_init_control(context);
#if defined(TPJT2) || defined(TPJT3) || defined(TPIP_EMULATOR)
    server_init_camera(context);
    server_init_sound(context);
#endif
    return context;
}

void server_destory(struct lws_context *context) {
    server_destroy_http();
    server_destroy_control();
#if defined(TPJT2) || defined(TPJT3) || defined(TPIP_EMULATOR)
    server_destroy_sound();
    server_destroy_camera();
#endif
    lws_context_destroy(context);
}


void sighandler(int sig){
    force_exit = true;
}

int server_main(){
    signal(SIGINT, sighandler);
    
    config_init("./config.json");
	if (tpip_init(json_string_value(config_get("ip"))) == TPIP_ERROR) {
		fprintf(stderr, "failed to init tpip library.\n");
		exit(-1);
	}
    printf("Target TPIP :  %s\n", tpip_get_target_ip());

	tpip_connect_control();

	if (tpip_connect_sound(tpip_get_target_ip(),
		TPIP_SOUND_SAMPLE_RATE_32000,
		1, 10) == TPIP_ERROR) {
		fprintf(stderr, "failed to connect tpip sound.\n");
		exit(-1);
	}
    
	if (robot_setup() < 0) {
		fprintf(stderr, "failed robot setup.\n");
		exit(-1);
	}

    puts("Robot setup OK.");

    struct lws_context *context = server_init();


    puts("Server setup OK.");

    struct interval_timer_t *timer = interval_timer_new();
    interval_timer_set_interval(timer, (void (*)(void))server_tasks_control, 10);
#if defined(TPJT2) || defined(TPJT3) || defined(TPIP_EMULATOR)
    interval_timer_set_interval(timer, (void (*)(void))server_tasks_camera, 33);
    interval_timer_set_interval(timer, (void (*)(void))server_tasks_sound, 100);    
#endif
    
    bool tpip_first_connected = false;
    
    while(!force_exit) {
        interval_timer_tick(timer);
        lws_service(context, 0);
        robot_loop();

        if(!tpip_first_connected) {
            if(tpip_is_control_connected()) {
                puts("TPIP Control Connection Established!");
                tpip_first_connected = true;
            }
        }
#ifdef _WIN32
        Sleep(1);
#else
        usleep(1000);
#endif
    }
    interval_timer_destroy(timer);
    server_destory(context);
    tpip_close();
    tpip_close_sound();
    
    return 0;
}
