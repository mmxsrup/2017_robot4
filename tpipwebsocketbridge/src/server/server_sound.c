/*
  server_sound.c

  音声中継サーバ

  受け入れるデータ

  {
  }
 
  送るデータ
 
  {
  }
 
*/

#include "server_camera.h"
#include "private.h"

#include "jansson.h"
#include "tpip.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

//
//
// private declearation
//
//

// struct

struct per_session_data__sound {
    int streaming_duration_ms;     // 送る周期の最大値
    time_t last_streamed_time;
    bool request_streaming;    // ストリーミングするかどうか
    bool request_status;   // 出力設定状態を送る時用のflg
};

// function

int callback_sound(struct lws *wsi,
                   enum lws_callback_reasons reason,
                   void *user,
                   void *in,
                   size_t len);

static bool update_user_setting(json_t *root,
                                struct lws *wsi,
                                struct per_session_data__sound *user);
static json_t *build_status_json();

// variable

static struct {
    int user_num;
    lws_send_buffer send_sound_buf;
    lws_send_buffer send_setting_buf;
    struct tpip_sound_data sound_int16_buf;
    struct tpip_sound_data sound_float32_buf;
    struct lws_context *context;
} local_;



//
//
// public
//
//

struct lws_protocols server_protcol_sound =  {
    "sound",
    callback_sound,
    sizeof(struct per_session_data__sound),
    65000,
};

bool server_init_sound(struct lws_context *context) {
    if(context == NULL) return false;
    local_.context = context;
    local_.user_num = 0;
    return true;
}

bool server_tasks_sound() {
    if (tpip_is_sound_in_connected() && local_.user_num > 0) {

        if(tpip_receive_sound(&local_.sound_int16_buf) == TPIP_OK) {
            if(tpip_sound_data_convert_int16_to_float32(&local_.sound_float32_buf,
                                                        &local_.sound_int16_buf) == TPIP_OK){
                
                lws_send_buffer_set(&local_.send_sound_buf,
                                    local_.sound_float32_buf.data,
                                    local_.sound_float32_buf.size);
                
                lws_callback_on_writable_all_protocol(local_.context,
                                                      &server_protcol_sound);
                return true;
            }
        }
    }
    return false;
}

bool server_destroy_sound() {
    local_.context = NULL;
    return true;
}

//
//
// private
//
//

// コールバック関数群

static int on_connection(struct lws *wsi,
                         struct per_session_data__sound *user_data) {
    user_data->request_streaming = true;
    user_data->request_status = false;
    user_data->last_streamed_time = clock();
    user_data->streaming_duration_ms = 1;
    
    if(local_.user_num == 0) tpip_clear_sound_in_buffer();
    local_.user_num += 1;
    
    return 0;
}

static int on_writable(struct lws *wsi,
                       struct per_session_data__sound *user_data) {
    
    if(user_data->request_status) {
        json_t *data = build_status_json();
        char *data_str = json_dumps(data, JSON_COMPACT);

        lws_send_buffer_set(&local_.send_setting_buf, data_str, strlen(data_str));
        lws_send_buffer_send(&local_.send_setting_buf, wsi, LWS_WRITE_TEXT);
        
        free(data_str);
        json_decref(data);
        
        user_data->request_status = false;        
    }
    else if(user_data->request_streaming) {
        time_t now = clock();
        time_t prev = user_data->last_streamed_time;
        int duration = user_data->streaming_duration_ms;
        
        if((double)(now - prev) / CLOCKS_PER_SEC * 1000 >= duration) {
            lws_send_buffer_send(&local_.send_sound_buf, wsi, LWS_WRITE_BINARY);
            user_data->last_streamed_time = now;
        }
    }
    return 0;
}

static int on_receive(struct lws *wsi,
                      struct per_session_data__sound *user_data,
                      void *in, size_t len) {
    if(lws_frame_is_binary(wsi)) {
        // バイナリ (= 音声データ)
        return 0;
        // 実装中
        // 複数のクライアントからデータが来たらどうするか？
        // クライアントのフレームレート等をどうするか -> 受け入れ固定 OR 事前に設定受け取る
        tpip_sound_data_reserve(&local_.sound_float32_buf, len);
        memcpy(local_.sound_float32_buf.data, in, len);
        local_.sound_float32_buf.size = len;
        local_.sound_float32_buf.format = TPIP_SOUND_FORMAT_F32;
        local_.sound_float32_buf.channel_num = 1; // TODO
        local_.sound_float32_buf.sample_rate = 0; // TODO
        
        if(tpip_sound_data_convert_float32_to_int16(&local_.sound_int16_buf,
                                                    &local_.sound_float32_buf) == TPIP_OK) {
            tpip_send_sound(&local_.sound_int16_buf);
        }
        
    } else {
        // テキスト (= JSON)
        const char *in_str = (const char*)in;
        json_error_t err;
        json_t *root = json_loads(in_str, 0, &err);
        if(!root) {
            fprintf(stderr, "%s error: on line %d: %s\n",
                    __func__, err.line, err.text);
            return -1; // reject & close
        }
        if(!update_user_setting(root, wsi, user_data)) {
            return -1;
        }
        
        json_decref(root);
    }
    return 0;
}

static int on_close(struct lws *wsi,
                    struct per_session_data__sound *user_data) {
    local_.user_num -= 1;
    return 0;
}


//
//
// 補助関数
//
//


static bool update_user_setting(json_t *root, struct lws *wsi,
                             struct per_session_data__sound *user) {
    if(!json_is_object(root)) return false;
    
    bool response = false;
    {
        json_t * value = json_object_get(root, "get");
        if(json_is_boolean(value)) {
            response = true;
            user->request_status = true;
        }
    }
    {
        json_t * value = json_object_get(root, "streaming");
        if(json_is_boolean(value)) {
            user->request_streaming = json_boolean_value(value);
        }
    }
    {
        json_t * value = json_object_get(root, "fps");
        if(json_is_integer(value)) {
            json_int_t fps = json_integer_value(value);
            user->streaming_duration_ms = 1.0 / fps * 1000;
        }
    }
	{
		json_t *value = json_object_get(root, "in_volume");
		if (json_is_number(value)) {
			double vol = json_number_value(value);
			tpip_set_sound_in_volume(vol);
			printf("in %f\n", vol);
		}
	}
    if(response) {
        lws_callback_on_writable(wsi);
    }
    return true;
}
    
static json_t *build_status_json() {
    json_t *root = json_object();
    json_object_set_new(root, "ip", json_string(tpip_get_target_ip()));
    json_object_set_new(root, "sample_rate", json_integer(tpip_get_sound_sample_rate()));
    json_object_set_new(root, "channel_num", json_integer(tpip_get_sound_channel_num()));
    json_object_set_new(root, "format", json_integer(TPIP_SOUND_FORMAT_DEFAULT));
    json_object_set_new(root, "in_status", json_string(tpip_is_sound_in_connected() ? "Connected" :
                                                       "Not Connected"));
    json_object_set_new(root, "out_status", json_string(tpip_is_sound_in_connected() ? "Connected" :
                                                        "Not Connected"));
    return root;
}




int callback_sound(struct lws *wsi, enum lws_callback_reasons reason,
                   void *user, void *in, size_t len) {
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            lwsl_notice("Connected client to sound stream server.\n");
            on_connection(wsi, (struct per_session_data__sound*)user);
            break;
        case LWS_CALLBACK_CLOSED:
            lwsl_notice("Disconnected client from sound stream server.\n");
            on_close(wsi, (struct per_session_data__sound*)user);
            break;
        case LWS_CALLBACK_SERVER_WRITEABLE:
            on_writable(wsi, (struct per_session_data__sound*)user);
            break;
        case LWS_CALLBACK_RECEIVE:
            on_receive(wsi, (struct per_session_data__sound*)user, in, len);
            break;
        case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
            dump_handshake_info(wsi);
            break;
    }
    return 0;
}
