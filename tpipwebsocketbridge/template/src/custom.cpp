/**
 * 
 * 各ロボット用の制御を書く
 * テンプレートではc++にしてあるが、Cでも良い
 *
 * 実装が必要な関数
 * int robot_setup()
 * int robot_loop()
 * int robot_on_receive(const json_t *rcv_json)
 * int robot_on_send(json_t *send_json)
 *
 */

#include "robot.h"          // include必須(robot_...が宣言されている)
#include "jansson.h"        // jsonパーサーライブラリ
#include "tpip.h"           // tpipライブラリのラッパー関数群
#include "tpip_json.h"      // tpipのデータをjson_tで取得・設定できる
#include "config_manager.h" // config.jsonから設定を読み込み・取得する
// #include "interval_timer.hpp" // （単一スレッドで）周期的に関数を実行するための簡易ライブラリ

/**
 *
 * TPIP API 初期化直後・サーバー起動前に呼ばれる関数
 * 初期位置等の設定をすると良い
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_setup() {

    return 0;
}


/**
 *
 * 定期的に呼ばれる関数
 * ただし呼ばれる間隔は他の処理の時間によって変動する
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_loop() {
    
    return 0;
}


/**
 *
 * 制御情報を受信した時に呼ばれる関数
 *
 * rcv_json : 受信したjsonデータ 向こうで解放されるので勝手にdecrefしないで。
 *
 * return : -1でエラー（接続を打ち切る）
 *           0で正常
 *
 */
int robot_on_receive(const json_t *rcv_json) {
    // TPIPの出力(Digital, PWM等)を受信データから設定する
    // これを呼ばないと出力は変更されない（->自分で制御可能）
    tpip_set_control_by_json(rcv_json);
    
    return 0;
}

/**
 *
 * センサ情報を情報を送信する前に呼ばれる関数
 *
 * send_json : 送信するjsonデータ 通信関連の最低限のデータが格納済み
 *             !! send_json に追加する値は全て所有権を譲渡しないといけない(_newな関数でaddする)
 * return : -1でエラー（送信しない）
 *           0で正常
 *
 */
int robot_on_send(json_t *send_json) {
    // TPIPのセンサー情報を格納する -> 必要な情報だけ選んで追加すること等が可能
    json_t *tpip_sensor_json = tpip_get_sensor_as_json();
    json_object_update(send_json, tpip_sensor_json);
    json_decref(tpip_sensor_json);

    
    return 0;
}


int main() {
    return server_main();
}
